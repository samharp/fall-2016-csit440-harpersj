//  Code by sharper with help from P. Swenson
//  12.15.2016
//
//  The following JavaScript Application gets user input through a quiz
//  The selected responses are recorded and the last results page shows recommendations on going greener.


var app = {};

window.addEventListener("load",function(){
    function myApp(){
        //my application code
        var button = document.getElementById("start");
        
        //loop to see what items were chosen on question pages
        button.addEventListener("click",function(){
            var answersforQuestion = document.querySelectorAll("input");
            var selectedAnswer;
            for(var x = 0; x < ((answersforQuestion.length)); x++){
                if(answersforQuestion[x].checked){
                    selectedAnswer = answersforQuestion[x].getAttribute('value');
                    var quesNumber = answersforQuestion[x].getAttribute('name');
                    window.sessionStorage.setItem(quesNumber,selectedAnswer);
                }
            }
        }, true);
    }
    
    
    function myResults(){
        //my application code
        var selected = [window.sessionStorage.getItem('q1'),
                window.sessionStorage.getItem('q2'),
                window.sessionStorage.getItem('q3'),
                window.sessionStorage.getItem('q4'),
                window.sessionStorage.getItem('q5'),
                window.sessionStorage.getItem('q6'),
                window.sessionStorage.getItem('q7'),
                window.sessionStorage.getItem('q8'),
                window.sessionStorage.getItem('q9'),
                window.sessionStorage.getItem('q10')];
        var tipBank = [
            [//q1 responses
                ['public-transport', 'Though public transportation is much better for the environment than everyone driving cars, most still burn fossil fuels, a leading cause to climate change.  Try walking or biking in order to save the planet as well as get fit in the process.', 'You usually get around by using public transportation'],
                ['car', 'Cars are convenient, but cars are also one of the biggest contributors to greenhouse gases being pushed into the air.  Over 15% of greenhouse gases are estimated to come from transportation.  Walking or biking can cut your carbon footprint.', 'You usually get around by car'],
                ['board', 'Cool dude!  Ride on to class now, and thanks for helping the environment!  Once that skateboard has fulfilled its duty, donate it!', 'You usually get around by skateboard, longboard, or Penny board'],
                ['foot', 'The first mode of transportation is arguably the best.  Slow down and smell the roses.  Once those walking/running shoes reach the end of their lifetime, either donate them or ask your recycling center if they accept old shoes.  Nike takes back athletic shoes and grinds them up for sports surfaces and also includes them in some of their gear.', 'You usually get around by foot'],
                ['bike', 'Biking is one of the fastest, foolproof ways to travel green.  Bikes are even cheaper to maintain than a car.  When you ride a bike to it’s last mile, instead of throwing it away, either donate it or try to sell it to a bike shop (depending on it’s condition).  Some bike shops will buy back old bikes and either repair them to resell or send them to a recycler.', 'You usually get around by bike']
            ],
            [//q2 responses
                ['all', 'Recycling plastics keep them from landfills, and gives new life to nonrenewable materials.  The next step is to reduce your plastics consumption.  Instead of buying a pack individual water bottles, buy one bottle you can refill each time you want water.', 'You recycle plastics all of the time'],
                ['most', 'Good job at recycling plastics, but try to boost your recycling rate.  Make it a habit to throw away plastics in the recycle bin instead of the trash.', 'You recycle plastics most of the time'],
                ['some', 'On the fence about recycling plastics?  Plastics can take anywhere from 25 to 1000 years to degrade in a landfill, depending on the type of plastic.  Recycle it and keep it out of the ground.', 'You recycle plastics sometimes'],
                ['rarely', 'The easiest way to recycle all of your goods is to make it into a habit.  Recycling plastics only means changing the bin you throw the trash in, but that small change can make a huge difference in the world.', 'You rarely recycle plastics'],
                ['never', 'Recycling plastics only means changing the bin you throw the trash in, but that small change can make a huge difference in the world.', 'You never recycle plastics']
            ],
            [//q3 responses
                ['all', 'Good job!  Recycling paper is an easy and widely available service in communities.  The next step is reducing your paper usage.  Not using the paper in the first place is the best way to cut back waste.', 'You recycle paper all of the time'],
                ['most', 'Boosting your paper recycling rates will keep the Earth clean for many generations to come!  Though paper is biodegradable, paper can remain intact for decades because of how tightly packed the landfills are.', 'You recycle paper most of the time'],
                ['some', 'Half of the time you recycle paper, and half of the time you don’t.  Why not throw all paper in the recycle bin , and soon enough you will not even think about it.', 'You recycle paper sometimes'],
                ['rarely', 'Recycling paper is a simple and effective way to cut the amount of trees used to make paper and keep landfills small.  Plus many recycling bins are ‘Mixed Recycling,’ so there is no need to even separate your recyclables.', 'You rarely recycle paper'],
                ['never', 'Recycling paper is the easiest and most popular type of recycling.  Almost all recycling centers take paper, so there is no excuse not to!', 'You never recycle paper']
            ],
            [//q4 responses
                ['all', 'Great!  No need to fill landfills more than they need to be!', 'You recycle cardboard all of the time'],
                ['most', 'Recycling cardboard is almost as easy as paper, and is almost as widely as accepted.  Start recycling all cardboard products you use and you will be perfect.', 'You recycle cardboard most of the time'],
                ['some', 'Landfills are ever growing with 55% of all trash going into them.  Let’s keep the landfills at bay, and in turn keep the world cleaner by recycling cardboard.', 'You recycle cardboard sometimes'],
                ['rarely', 'Landfills are ever growing with 55% of all trash going into them.  Let’s keep the landfills at bay, and in turn keep the world cleaner by recycling cardboard.', 'You rarely recycle cardboard'],
                ['never', ' Cardboard is one of the most biodegradable products in landfills, but it also takes up space.  Landfills are ever-growing and to keep the Earth from becoming a giant trash heap, let’s make use of the materials we have.', 'You never recycle cardboard']
            ],
            [//q5 responses
                ['all', 'Great!  Educating yourself on the environment is the best way to learn how you can change it.  Hopefully the movies/TV shows are on an energy-efficient TV though...', 'You watch TV or movies that cover environmental topics all of the time'],
                ['most', 'Great!  Educating yourself on the environment is the best way to learn how you can change it.  Hopefully the movies/TV shows are on an energy-efficient TV though..', 'You watch TV or movies that cover environmental topics most of the time'],
                ['some', 'Taking the time to want to learn more about the environment is a good step towards changing your green habits.  All movies and TV shows are different though, so you may want to try watching a different program in order to educate yourself in a more enjoyable way.', 'You watch TV or movies that cover environmental topics sometimes'],
                ['rarely', 'Granted some documentaries are boring, there is a lot of content out there that is interesting and eye-opening.  National Geographic, Discovery Channel, and Nature are all programs that have great programming for those that want to learn more about the environment.', 'You rarely watch TV or movies that cover environmental topics'],
                ['never', 'Granted some documentaries are boring, there is a lot of content out there that is interesting and eye-opening.  National Geographic, Discovery Channel, and Nature are all programs that have great programming for those that want to learn more about the environment.  Give one of these programs a chance, and maybe you will find yourself enjoying the programming.', 'You never watch TV or movies that cover environmental topics']
            ],
            [//q6 responses
                ['no-can', 'Littering is littering, no matter what the condition.  Keep ahold of your trash just until you can find a recycle bin or trash can.', 'You only litter because there is no recycle bin or trash can in sight'],
                ['lazy', 'Take the extra time and effort to keep the planet clean.  You’ll feel good about it too!', 'You only litter because you do not want to walk over to it'],
                ['car', 'Dirty cars are gross, but just because you cannot find a recycle bin or trash can right this second does not mean you have to turn your car into a landfill.  Just empty the trash out of your car when you park.', 'You only litter because you do not want to fill your car with trash'],
                ['biodegrad', 'Even though some products may have biodegradable packaging, it does not mean the packaging will decompose overnight.  Food can decompose faster than plastics, but food litter can also disrupt wildlife eating habits and behavior.', 'You only litter because it is biodegradable'],
                ['accident', 'Though accidents happen, try your best not to litter at all.  Littering causes land and water pollution that can kill wildlife.', 'You only litter because you accidently dropped it'],
                ['never', 'Good job!  If everyone followed you in a litter-free zone, the world would be a much cleaner place.', 'You never litter']
            ],
            [//q7 responses
                ['yes', 'Though water cannot be created or destroyed, it can become unusable by water pollution.  Pulling clean water from an aquifer just to have it flow downstream and possibly become polluted is wasteful.', 'You leave the water running when you brush your teeth'],
                ['no', 'Good job!  Keep those teeth clean and save our fresh water.', 'You turn the water off when you brush']
            ],
            [//q8 responses
                ['yes', 'Microbeads are extremely dangerous for fish and other animals, especially marine life.  They may think it is food, and ingest the plastic beads.  The beads act like sponges and may contain harmful chemicals that poison fish or other animals that eat them.', 'You use microbead products'],
                ['no', 'You must have gotten the memo that these plastic beads were dangerous for marine life and pollute our great lakes and oceans!', 'You do not use microbead products']
            ],
            [//q9 responses
                ['yes', 'Turning off the lights to a room after you leave is a great way to save electricity!  To save even more electricity, don’t turn the lights on at all!', 'You turn the lights off when leaving a room'],
                ['no', 'Turning off the lights when you leave a room can save electricity, but also, who are you helping by keeping the lights on?', 'You do not turn the lights off when leaving a room']
            ],
            [//q10 responses
                ['yes', 'That’s great!  Improving your knowledge of the environment is that much better when you are also earning credits!', 'You have taken an Environmental topics class'],
                ['no', 'Even though an Environmental Issues class may not be required, think about taking one as a General Studies to improve your knowledge of the environment.', 'You have not taken an environmental topics class']
            ]
        ];
        var card, tip, content, testcontent, title, titleContent;
        
        //Match what items were picked to bank
        for(var x = 0; x < selected.length; x++){
            tip = document.createElement("p");
            for(var i = 0; i < 8; i++){
                if(selected[x] == tipBank[x][i][0]){
                    content = document.createTextNode(tipBank[x][i][1]);
                    titleContent = document.createTextNode(tipBank[x][i][2]);
                    break;
                }
            }
            
            //apply cards with content to page
            title = document.createElement("h2");
            card = document.createElement("div");
            title.appendChild(titleContent);
            tip.appendChild(content);
            card.appendChild(title);
            card.appendChild(tip);
            document.getElementById("tray").appendChild(card);
        }
        
        //randomize design elements
        var divlist = document.querySelector('section').children;
        //divlist[Math.floor(Math.random() * divlist.length)].style.border = '2px dotted #00A246';
        //divlist[Math.floor(Math.random() * divlist.length)].style.border = '0';
        //divlist[Math.floor(Math.random() * divlist.length)].style.border = '0';
        //divlist[Math.floor(Math.random() * divlist.length)].style.border = '0';
        
        //randomize the order of cards
        divlist = document.querySelector('section');
        for(var y = divlist.children.length; y >=0; y--){
            divlist.appendChild(divlist.children[Math.random() * y | 0]);
        }
    }
    
    
    var AjaxMyApp = function(router){
        //listen for afterresponse so that we can cache requests not currently stored
        router.subscribe("afterresponse",function(msg){//data
            if(msg.href.indexOf("ques")  > -1 || msg.href.indexOf("ques")  > -1){
                //window.history.pushState(null,"HelpEarth Quiz", "quiz.html");
                myApp();
            }
            if(window.location.href.indexOf("results.html") >-1){
                myResults();
            }
            if(window.location.href.indexOf("quiz.html") >-1){
                window.location = "ques-1.html";
            }
        });
        
        //this will call your app if you start oge (requires fixes on 274-276 of day3.js in this project)
        if(window.location.href.indexOf("ques") > -1){
            myApp();
        }
    };
    
    //create a router instance and decorate it
    var router = new app.ApplicationRouter("main");
    router.decorate(app.AjaxFadeEffect);
    router.decorate(app.AjaxHistory);
    router.decorate(app.AjaxCache);
    router.decorate(AjaxMyApp);//don't forget to do this
},false);
    
