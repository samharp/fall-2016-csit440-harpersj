/**
 * Wrapping application Code in load event for both a namespace and to make sure content is ready
 */
(function(namespace){
    
    /**
     * Object from 2_object_oriented
     *  modified to stop processing if a message handler returns false
     */
    var EventPublisher = function(){
        var subscriptions = {};
        
        this.publish = function(type,msg){
            if(subscriptions[type] !== undefined){
                for(var i=0; i<subscriptions[type].length; i++){
                    //wrapped the execution of a subscription function in an if.  Returning false causes the loop to end
                    if(subscriptions[type][i].call(this,msg) === false){
                        return false;
                    }
                }
            }
            return true;
        };
        
        this.subscribe = function(type,callback){
            if(typeof callback === 'function'){
                if(subscriptions[type] === undefined){
                    subscriptions[type] = [];
                }
                subscriptions[type].push(callback);
            }
        };
        
        this.unsubscribe = function(type,callback){
            if(subscriptions[type] !== undefined){
                for(var i=0; i<subscriptions[type].length; i++){
                    if(subscriptions[type][i] === callback){
                        subscriptions[type][i].splice(i,1);
                        return;
                    }
                }
            }
        };
    };


    /**
     * Wrapped the xmlhttp api in a function
     */
    function ajax(url,options){
        var xmlhttp = new XMLHttpRequest(),
            debugFunction = function(data){
                console.log(data);
            }
        
        //if an option is not set load a default value
        options = options || {};
        options.method = options.method || "GET";
        options.success = options.success || debugFunction;
        options.failure = options.failure || debugFunction;
        options.data = options.data || "";
        options.timeout = options.timeout || 30000;
        options.id = options.id || Date.now() + url;
        
        //Handle requests that never complete
        var timeout = setTimeout(function(){//handle no response
            options.failure("Server Timed Out", options.id);
        },options.timeout);
        
        //readystatechange fires once at each step in the ajax, but we only care about 4 (complete)
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState === 4){//server request is complete
            
                //stop the setTimeout from firing since the request completed
                clearTimeout(timeout);//clear the timeout if a request comes back
            
                if(xmlhttp.status === 200){// request was successful
                    options.success(xmlhttp.responseText, options.id);
                }else{                      //request was not successful
                    if(xmlhttp.responseText !== ''){//The server sent a response for the user
                        options.failure(xmlhttp.responseText, options.id);
                    }else{//default error message
                        options.failure("Error " + xmlhttp.status + ' ' + xmlhttp.statusText, options.id);
                    }
                }
            }
        };
        
        xmlhttp.open(options.method,url,true);
        xmlhttp.setRequestHeader("x-requested-with","XMLHttpRequest");//we need this so the server knows an asyncronous request is being made
        xmlhttp.send(options.data);
        return options.id; 
    }
    
    
    /**
     * Handles ajax requests made by the user
     */
    namespace.ApplicationRouter = function(contentId){
        var o = this,
            content = document.getElementById(contentId),//where the dynamic content is to be placed
            transactions = {}, //list of any transactions currently waiting for a response
            lastTransactionId = 0; //keep track of the last number used so we can issue a new number the next time ajax is called
        
        var phases = {//Provide hooks to do custom code before and after each step in the ajax
            beforeRoute:function(msg){
                return o.publish("beforeroute",msg);
            },
            route:function(msg){//actual request
                ajax(msg.href,{
                    success:o.onresponse,
                    failure:o.onresponse,
                    id:msg.id
                });
                return true;
            },
            afterRoute:function(msg){
                return o.publish("afterroute",msg);
            },
            beforeResponse:function(msg){
                return o.publish("beforeresponse",msg);
                return true;
            },
            response:function(msg){//actual response
                content.innerHTML = msg.data;
                return true;
            },
            afterResponse:function(msg){
                return o.publish("afterresponse",msg);
            }
        };
        
        /**
         * Use this method to add classes that contain extra functionality
         */
        o.decorate = function(decorator){
            if(typeof decorator === 'function'){
                new decorator(o);
            }
        };
        
        /**
         * Provide a way to cleanly delete a tranaction
         */
        o.clearTransaction = function(transactionId){
            delete transactions[transactionId];
        };
        
        /**
         * Get the element used in the page to hold dynamic content
         */
        o.getContent = function(){
            return content;  
        };
        
        /**
         * Get a unique transaction id by incrementing the last one, but if for some reason we reach the max integer value reset to 1
         */
        o.getNextTransactionId = function(){
            if(lastTransactionId < Number.MAX_SAFE_INTEGER){
                lastTransactionId++;
            }else{
                lastTransactionId = 1;
            }
            return lastTransactionId;
        };
        
        /**
         *  Start an ajax request
         */
        o.onroute = function(href){
            var msg = {//state object used for the whole transaction
              id:o.getNextTransactionId(),
              href:href,
              data:""
            };
            
            //Store the message to be used by the response under the transaction number
            transactions[msg.id] = msg;
            
            //Trigger all phases of the ajax request
            if(phases.beforeRoute(msg)){
                if(phases.route(msg)){
                    phases.afterRoute(msg);
                }
            }
        };
        
        /**
         * Start an ajax response
         */
        o.onresponse = function(data, transactionId){
            var msg = undefined;
 
            if(transactionId && transactions[transactionId]){//if a transaction exists use it
                msg = transactions[transactionId];
                msg.data = data;
                
                o.clearTransaction(transactionId);//transaction is no longer needed clean it up
            }else{//if a transaction doesn't exist recreate one
                msg = {
                    id:transactionId,
                    href:"",
                    data:data
                };
            }
            
            //trigger all phases of the ajax response
            if(phases.beforeResponse(msg)){
                if(phases.response(msg)){
                    phases.afterResponse(msg);
                }
            }
        };
        
        //Set it up basically this is the code that initializes the router
        if(content !== null){
            EventPublisher.call(o);
            
            content.classList.add("in");
            
            window.addEventListener("click",function(e){
                var a = e.target;
                
                if(a.href !== undefined && a.href.indexOf(window.location.host) > -1){
                    e.preventDefault();
                    o.onroute(a.href);
                }
            },false);
        }
    };
    namespace.ApplicationRouter.prototype = Object.create(EventPublisher.prototype);
    namespace.ApplicationRouter.constructor = EventPublisher.constructor;
    
    
    /**
     *  Decorator object used to fade the content area in and out
     */
    namespace.AjaxFadeEffect = function(router){
        
        //Before doing any routing work hide the content area
        router.subscribe("beforeroute",function(msg){
            router.getContent().classList.remove("in");
            router.getContent().classList.add(("out"));
        });
        
        //after the new content is placed in the content area show it
        router.subscribe("afterresponse",function(msg){
            setTimeout(function(){//Slowing the code down so the transitions fully run
                router.getContent().classList.remove("out");
                router.getContent().classList.add(("in"));
            },100);
        });
    };
    
    /**
     * Decorator object used to wire up the back button.
     */
    namespace.AjaxHistory = function(router){
        
        //Once a route is executed set it into histor
        router.subscribe("afterroute",function(msg){
            window.history.pushState({
                href:msg.href
            },"",msg.href);
        });
        
        //If the back button is pushed cause the router to restore the application state
        window.addEventListener("popstate",function(e){
            router.onroute(e.state.href);
        },false);
        
        //Don't forget we have to set the initial route so there is some page to go back to after the first click.
        window.history.pushState({
            href:window.location.href
        },"",window.location.href);
    };
    
    /**
     * Decorator object reduces calls to the server by storing requests for 30 seconds
     */
    namespace.AjaxCache = function(router){
        var cache = {};
        
        //before making the ajax request determine if we have this in cache
        router.subscribe("beforeroute",function(msg){//href
            var data = cache[msg.href];

            if(data){//We have in cache so cancel the request and trigger the response
                router.publish("afterroute",msg);//don't skip the afterroute phase
                router.onresponse(data, msg.id);//fire the response with cached data.
                return false;//says don't make ajax request
            }
        });
        
        //listen for afterresponse so that we can cache requests not currently stored
        router.subscribe("afterresponse",function(msg){//data
            if(!cache[msg.href]){
                cache[msg.href] = msg.data;
         
                setTimeout(function(){//only keep for 30 seconds
                    delete cache[msg.href];
                },30000);
            }
        });
    };

    


}(app));