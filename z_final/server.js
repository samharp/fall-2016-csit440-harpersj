var http = require("http"),
    url = require("url"),
    fs = require("fs"),
    path = require("path");
    
var mimeTypes = {
    html:"text/html",
    css:"text/css",
    png:"imgage/png",
    pdf:"application/pdf",
    xml:"application/xml",
    jpg:"image/jpeg",
    gif:"image/gif",
    docx:"application/vnd.openxmlformats-officedocument.wordprocessing.document"
};

var templateCache = null;

function getFileContents(filepath,onsuccess,onerror){
    fs.readFile(filepath,'utf-8',function(err,data){
        if(err){
            onerror(err);
        }else{
            onsuccess(data);
        }
    });
}

function wrapContent(fileData,onsuccess,onerror){
    if(templateCache !== null){
        console.log(templateCache);
        var html = templateCache.replace("<!--CONTENT-->",fileData);
        onsuccess(html);
    }else{
        getFileContents(__dirname + '/template.html',function(templateData){
            templateCache = templateData;
            var html = templateCache.replace("<!--CONTENT-->",fileData);
            onsuccess(html);
        },function(err){
            onerror(err);
        });
    }
}

http.createServer(function(request,response){
    console.log("Request Recieved " + request.url);
    
    var pathname = url.parse(request.url).pathname,
        extension = null,
        filepath = null;
    
    pathname = (pathname === "/") ? "/index.html" : pathname;
    extension = path.extname(pathname).split(".")[1];
    
    //is this an html request
    if(extension === 'html'){
        filepath = __dirname + '/html' + pathname;
    }else{
        filepath = __dirname + pathname;
    }
    
    fs.stat(filepath,function(err,stats){
        if(err){
            console.log(err);
            response.writeHead(404,{});
            response.end("file not found");
        }else{
            var mimeType = mimeTypes[extension] || "text/html",
                filestream = null;
                
            if(extension === 'html' && request.headers['x-requested-with'] !== 'XMLHttpRequest'){
                getFileContents(filepath,function(data){ 
                    wrapContent(data,function(wrappedHTML){
                        response.writeHead(200,{
                            "Content-Type" : mimeType
                        });
                        response.end(wrappedHTML);
                    },function(err){
                        console.log(err);
                        response.writeHead(500,{});
                        response.end("Sorry. We were unable to process your request");
                    });
                },function(err) {
                    console.log(err);
                    response.writeHead(500,{});
                    response.end("Sorry. We were unable to process your request");
                });
            }else{
                response.writeHead(200,{
                    "Content-Type" : mimeType
                });
                filestream = fs.createReadStream(filepath);
                filestream.pipe(response);
            }
        }
    });
    
    
    
}).listen(process.env.PORT,process.env.IP);
console.log("Server Started");