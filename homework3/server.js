var http = require("http"),
    url = require("url"),
    fs = require("fs"),
    path = require("path");
    
var mimeTypes = {
    html:"text/html",
    css:"text/css",
    png:"img/png"
}


http.createServer(function(request,response){
    console.log("Request Recieved " + request.url);
    
    var pathname = url.parse(request.url).pathname,
        filepath = null;
        
    pathname = (pathname === "/") ? "/index.html" : pathname;
    filepath = __dirname + pathname;
    fs.stat(filepath,function(err,stats){
        if(err){
            console.log(err);
            response.writeHead(404,{});
            response.end("file not found");
        }
        else{
            var extension = path.extname(filepath).split(".")[1],
                mimeType = mimeTypes[extension];
                filestream = fs.createReadStream(filepath);
            
            response.writeHead(200,{
                "Content-Type" : mimeType
            });
            filestream.pipe(response);
        }
    });
    
}).listen(process.env.PORT,process.env.IP);

console.log("Server Started");
