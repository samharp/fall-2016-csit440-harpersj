//  Code by sharper with A LOT of help from P. Swenson
//  10.28.2016
//
//  The following JavaScript Application takes data from a table and dchanges its format
//  With new format, rows become columns and columns become rows.
//  Average scores for each student are also calculated, and students are orgranized by highest average to lowest.

//namespace (self executing function)
(function(tableSwitch){
    
    //get table from HTML
    var table = document.getElementById("grades");
    
    //get rows from HTML
    var rows = table.querySelectorAll("tr");
    //console.log(rows);
    
    //get rows into array
    rows = Array.prototype.slice.apply(rows);
    
    for(var i = 0; i < rows.length; i++){
        var arr = [];
        for(var j = 0; j < rows[i].children.length; j++){
            
            //assigns the cells to the temporary array one by one
            //copies whole node to node property of object
            //sets the value, inside html tags, to value property of object
            arr.push({
               node: rows[i].children[j].cloneNode(true),
               val: rows[i].children[j].innerHTML
            });
        }
        
        //push temporary array (with current row) to rows array, making table into a full 2d Array
        rows[i] = arr;
        
    }
    
    //Check if all data from table made it to rows array
    //console.log(rows);
    
    //create empty array with correct size of 2d array
    var pivot = [];
    for(var k = 0; k < rows.length; k++){
        pivot[k] = [];
    }
    
    for(i = 0; i < rows.length; i++){
        
        for(j = 0; j < rows[i].length; j++){
            
            //swap rows to columns, columns to rows into new array
            pivot[j][i] = rows[i][j];
        }
    }
    
    //check if rows pivoted
    //console.log(pivot);
    
    //loop through cells with numbers.  Exclude top row (test title), and first column (names)
    for(i = 1; i < pivot.length; i++){
        var total = 0;
        
        for(j = 1; j < pivot[i].length; j++){
            
            //if last column (average column)
            if(j === (pivot[i].length - 1)){
                
                //make last cell equal to average
                //average is calculated by dividing total by number of columns in row with numbers.  In this program, it is length minus two (compensates for the average column and names column)
                var avg = total/(pivot[i].length - 2);
                
                //set the node and val so code doesn't blow up
                pivot[i][j].val = avg;
                pivot[i][j].node.innerHTML = avg;
                
            }
            else{
                
                //add values from row (all students test scores) to total
                total += parseInt(pivot[i][j].val);
            }
        }
    }
    
    //check if averages show in array
    //console.log(pivot);
    
    //if the value is less than/more than the previous average, then it will sort it accordingly.
    //then < sorts the rows by largest to smallest, > sorts the rows smallest to largest
    pivot.sort(function(a,b){
        return (a[a.length-1].val < b[b.length-1].val)? 1 : -1;
    });
    
    //create new table
    var newTable = document.createElement("table");
    
    //create new row each time the for loop goes
    for(var i = 0; i < pivot.length; i++){
        var tr = document.createElement("tr");
        
        //create new table data inside table row
        for(var j = 0; j < pivot[i].length; j++){
            if(pivot[i][j] !== undefined){
                
                //checks if element is table header or table data
                //with table header, to make unique, just add _1
                if(pivot[i][j].node.nodeName === "TH"){
                    pivot[i][j].node.id += "_1";
                }
                
                //if td, to make unique, space is delimeter
                else{
                    var headers = pivot[i][j].node.headers.split(" ");
                    pivot[i][j].node.headers = headers[0] + "_1 " + headers[1] + "_1";
                }
                
                //fill tr variable with the newly ordered objects (put cells from 2d array into tr array with latest changes to id or headers)
                tr.appendChild(pivot[i][j].node);
            }
        }
        
        //put tr variable into the table.  Inside outer loop so each row will be put into table each time
        table.appendChild(tr);
    }
    
    //append new table to the document, inside the body tag
    document.body.appendChild(newTable);
    
    //lazy loading technique
    //good so if you make widget, simplifies installation of widget
    //so consumer doesn't have to code
    var link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = "css/styles.css";
    document.head.appendChild(link);
    
}());
