//  Code by sharper with A LOT of help from P. Swenson
//  11.09.2016
//
//  The following JavaScript Application takes an image and cuts it into pieces.
//  The pieces are jumbled and are draggable.  
//  The pieces can then be re-arranged by the user into a square and become a complete image

//runs the program when the window loads
window.addEventListener("load", function(){
    
    //put divs (puzzle board and pieces start) and button as a variable
    var button = document.getElementById("reset");
    var puzzleBoard = document.getElementById("puzzle_board");
    var piecesStart = document.getElementById("pieces_start");
    var pieces = undefined;
    var spaces = undefined;
    
    //isAssembled checks if the puzzle is correctly put together
    //called each time the user drops a piece
    var isAssembled = function(spaces){
        for(var i = 0; i < spaces.length; i++){
            var n = i + 1;
            if(spaces[i].children.length === 0 || spaces[i].children[0].id !== "piece_" + n){
                return;
            }
        }
        setTimeout(function(){
            alert("Congratulations, you put it together!");
        }, 100);
    }
    
    //if everything needed to effectively run the program exists
    if(button !== null && puzzleBoard !== null  && piecesStart !== null){
        //Puts all pieces from inside puzzle to puzzle pieces array
        pieces = puzzleBoard.querySelectorAll("div[draggable='true']");
        spaces = puzzleBoard.children;
        
        for(var i = 0; i < pieces.length; i++){
            pieces[i].addEventListener("dragstart", function(e){
                e.dataTransfer.setData("id", this.id);
            }, false);
        }
        
        for(var j = 0; j < spaces.length; j++){
            spaces[j].addEventListener("dragover", function(e){
                e.preventDefault();
            }, false);
        
            spaces[j].addEventListener("drop", function(e){
                if(this.children.length === 0){
                    e.preventDefault();
                    var id = e.dataTransfer.getData("id");
                    this.appendChild(document.getElementById(id));
                    isAssembled(spaces);
                }
            }, false);
        }
        
        //able to drag back items
        piecesStart.addEventListener("dragover", function(e){
            e.preventDefault();
        }, false);
        
        //what happens when items are dropped back
        piecesStart.addEventListener("drop", function(e){
            e.preventDefault();
            var id = e.dataTransfer.getData("id");
            this.appendChild(document.getElementById(id));
        }, false);
        
        button.addEventListener("click",function(){
            //put all of the seperate pieces as seperate array elements
            var arr = Array.prototype.slice.call(pieces);
            while(arr.length > 0){
                var index = Math.floor(Math.random() * arr.length);
                
                //randomize order, put piece into a random spot
                piecesStart.appendChild(arr.splice(index, 1)[0]);
            }
        }, false);
    }
}, false);