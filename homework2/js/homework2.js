//  Code by sharper with help from P. Swenson
//  10.20.2016
//
//  The following JavaScript Application follows the joke-filled conversation between Bob and Steve.
//  Both joke tellers take turns telling jokes, but also listen to the other teller.
//  Once either of the joke tellers runs out of jokes, they will stop the back and forth.

(function(jokesTop){
    
    //EventPublisher provided by P. Swenson//
    var EventPublisher = function(){
        var subscriptions = {
            onevent:[
                function(msg){},
                function(msg){}
            ]
        };
        this.publish = function(type,msg){
            if(subscriptions[type] !== undefined){
                for(var i=0; i<subscriptions[type].length; i++){
                    subscriptions[type][i].call(this,msg);
                }
            }
        };
        this.subscribe = function(type,callback){
            if(typeof callback === 'function'){
                if(subscriptions[type] === undefined){
                    subscriptions[type] = [];
                }
                subscriptions[type].push(callback);
            }
        };
        this.unsubscribe = function(type,callback){
            if(subscriptions[type] !== undefined){
                for(var i=0; i<subscriptions[type].length; i++){
                    if(subscriptions[type][i] === callback){
                        subscriptions[type][i].splice(i,1);
                        return;
                    }
                }
            }
        };
    };
    
    //create jokes as objects
    var joke01 = {
        who: "Canoe",
        punchline: "Canoe come and hang out later?"
    };
    var joke02 = {
        who: "Who",
        punchline: "That's what an owl says!"
    };
    var joke03 = {
        who: "Lettuce",
        punchline: "Lettuce in it's cold out here!"
    };
    var joke04 = {
        who: "Honey Bee",
        punchline: "Honey Bee a dear and bring me my phone."
    };
    var joke05 = {
        who: "Wooden Shoe",
        punchline: "Wooden Shoe let me in?"
    };
    var joke06 = {
        who: "Broken Pencil",
        punchline: "Nevermind, it's pointless..."
    };
    var joke07 = {
        who: "Cow says",
        punchline: "No, a cow says moo!"
    };
    var joke08 = {
        who: "Mikey",
        punchline: "Mikey won't work for some reason."
    };
    var joke09 = {
        who: "Atch",
        punchline: "Bless you!"
    };
    var joke10 = {
        who: "I am",
        punchline: "You don't know your name by now?"
    };
    var joke11 = {
        who: "Ya",
        punchline: "I'm glad to see you too!"
    };
    var joke12 = {
        who: "Figs",
        punchline: "Figs the doorbell, it's broken."
    };
    var joke13 = {
        who: "Boo",
        punchline: "Don't cry, it's just me!"
    };
    var joke14 = {
        who: "Iva",
        punchline: "Iva sore hand from knocking so much."
    };
    var joke15 = {
        who: "A little old lady",
        punchline: "I didn't know you could yodel!"
    };
    var joke16 = {
        who: "Dozen",
        punchline: "Dozen anybody want to let me in?"
    };
    var joke17 = {
        who: "Noah",
        punchline: "Noah good place we can go eat?"
    };
    var joke18 = {
        who: "Harry",
        punchline: "Harry up we have to go!"
    };
    var joke19 = {
        who: "Theodore",
        punchline: "Theodore is stuck and won't open, let me in!"
    };
    var joke20 = {
        who: "Anita",
        punchline: "Anita borrow your car."
    };
    //put jokes into array
    var jokes = [
        joke01, joke02, joke03, joke04, joke05, joke06, joke07, joke08, joke09, joke10, joke11, joke12, joke13, joke14, joke15, joke16, joke17, joke18, joke19, joke20
    ];
    var toldWho = "";
    
    //constructor
    function JokeTeller(tellerName){
        var jokeBank = [];
        var name = tellerName;
        
        this.setName = function(){
            name = tellerName;
        };
        this.getName = function(){
            return name;
        };
        this.getJokeBankLength = function(){
            return jokeBank.length;
        }
        this.teach = function(index){
            jokeBank.push(jokes[index]);
        };
        this.listen = function(listenTo){
            var o = this;
            listenTo.subscribe("afterknock",function(){
                console.log(name + ": Who's there?");
            });
            listenTo.subscribe("afterwho",function(){
                console.log(name + ": " + toldWho + " who?");
            });
        };
        this.tell = function(index){
            var o = this;
            var ranNum = 0;
            while(true){
                ranNum = Math.floor(Math.random() * jokeBank.length);
                if(jokeBank[ranNum] !== undefined){
                    break;
                }
            }
            console.log(name + ": Knock knock.");
            o.publish("afterknock");
            toldWho = jokeBank[ranNum].who;
            console.log(name + ": " + jokeBank[ranNum].who + ".");
            o.publish("afterwho");
            console.log(name + ": " + jokeBank[ranNum].punchline);
            console.log("----------");
            jokeBank.splice(ranNum,1);
            o.publish("afterpunch");
        }
        this.list = function(){
            return jokeBank;
        }
        EventPublisher.call(this);
    };
    
    JokeTeller.constructor = Object.call(EventPublisher);
    JokeTeller.prototype = EventPublisher.prototype;
    
    //application
    var KnockKnockApp = function(jokesTop){
        this.start = function(){
            var ranNum = 0;
            var teller1 = new JokeTeller("Bob");
            var teller2 = new JokeTeller("Steve");
            
            //teach jokes to the tellers
            for(var i = 0; i < jokes.length; i++){
                ranNum = Math.floor(Math.random() * 20);
                if(ranNum % 2 === 0){
                    teller1.teach(i);
                }
                else{
                    teller2.teach(i);
                }
            }
            
            teller1.listen(teller2);
            teller2.listen(teller1);
            
            var y = 0;
            while(teller1.getJokeBankLength() !== 0 && teller2.getJokeBankLength() !== 0/*y < teller1.getJokeBankLength() && y < teller2.getJokeBankLength()*/){
                teller1.tell();
                teller2.tell(y);
                y++;
            }
            if(teller1.getJokeBankLength() === 0){
                console.log(teller1.getName() + ": I'm all out of jokes.");
            }
            else{
                console.log(teller2.getName() + ": I'm all out of jokes.");
            }
        };
    };

//Window listener provided by P. Swenson    
window.addEventListener("load", function(){
        window.app = new KnockKnockApp(jokesTop);
    }, false);
    
}());
